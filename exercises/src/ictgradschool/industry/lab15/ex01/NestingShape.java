package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rher490 on 8/01/2018.
 */
public class NestingShape extends Shape {
    //stores the NestingShape's children
    private List<Shape> children = new ArrayList<>();


    /*Constructors with varying parameters*/
    public NestingShape() {
        super();
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }
    /*end constructor list*/

    //moves the Nesting shape along with its children
    @Override
    public void move(int width, int height) {
        super.move(width, height);
        //NS has a list children, move to all children?
        for (Shape c : children) {
            c.move(this.fWidth, this.fHeight);
        }
    }

    @Override
    public void paint(Painter painter) {
        painter.drawRect(fX, fY, fWidth, fHeight);
//        System.out.println();
        painter.translate(this.fX, this.fY);
        for (Shape c : children) {

            c.paint(painter);
        }
        painter.translate(-(this.fX), -(this.fY));
    }

    /*adds a child to the NestingShape
    * @param Shape to be added as a child*/
    public void add(Shape child) throws IllegalArgumentException {
        //checks all that the child's initial position and size is inside the NestingShape's boundaries
//        if(child.getX()+child.getWidth()>this.getX()+this.getWidth()||child.getY()+child.getHeight()>this.getY()+this.getHeight()||child.getX()<this.getX()||child.getY()<this.getY()){
//            throw new IllegalArgumentException();
//        }

        if (child.getX() + child.getWidth() > this.getX() + this.getWidth()) {
            System.out.println("Right Boundary");
            System.out.println(child.getX() + child.getWidth() > this.getX() + this.getWidth());
            throw new IllegalArgumentException();
        } else if (child.getY() + child.getHeight() > this.getY() + this.getHeight()) {
            System.out.println("Bottom Boundary");
            System.out.println(child.getY() + child.getHeight() > this.getY() + this.getHeight());
            throw new IllegalArgumentException();
        } else if (child.getX() < this.getX()) {
//            System.out.println(child.getClass());
            System.out.println("Left Boundary: Child (" + child.getX()+", "+child.getY() + ") ParentX "+ this.getX()+", "+this.getY() + ")");
            throw new IllegalArgumentException();
        } else if (child.getY() < this.getY()){
            System.out.println("Top Boundary");
            System.out.println(child.getY()<this.getY());
            throw new IllegalArgumentException();
        }

        //checks that the child is an orphan before the NestingShape adopts it, otherwise it's illegal
        if (child.parent == null) {
            child.parent = this;
            children.add(child);
        } else {
            System.out.println(child + " has a parent: "+child.parent);
            throw new IllegalArgumentException();
        }

    }

    /* removes the child from NestShape's children
    *  @param Shape child to be adopted*/
    public void remove(Shape child) {
        //checks that NestShape did adopt child
        if (child.parent.equals(this)) {
            children.remove(child);
            child.parent = null;
        } else {
            System.out.println(child + "is not a child of " + this);
        }
    }

    /*Finds the Shape of the Nesting Shapes nth child*/
    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
        return children.get(index);
    }

    /*returns the number of children*/
    public int shapeCount() {
        return children.size();
    }

    /*returns the index of the child in the list*/
    public int indexOf(Shape child) {
        return children.indexOf(child);
    }

    /*checks whether shape is a a child of NestingShape*/
    public boolean contains(Shape child) {
        if (children.indexOf(child) >= 0) {
            return true;
        } else {
            return false;
        }
    }

}
