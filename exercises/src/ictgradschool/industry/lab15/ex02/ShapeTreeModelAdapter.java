package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rher490 on 15/01/2018.
 * I implemented the TreeModel into the ShapeTreeModelAdapter and then edited the methods that needed to be implemented
 * The methods are based on the TreeModelAdapter in this labs examples
 */
public class ShapeTreeModelAdapter implements TreeModel {

    private NestingShape root;

    private List<TreeModelListener> _treeModelListeners;

    public ShapeTreeModelAdapter(NestingShape root) {
        this.root = root;
        _treeModelListeners = new ArrayList<TreeModelListener>();
    }

    public Object getRoot() {
        return root;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if (parent instanceof NestingShape) {
            NestingShape s = (NestingShape) parent;
            return s.shapeAt(index);
        }
        return null;
    }

    @Override
    public int getChildCount(Object parent) {
        int index = -1;
        if (parent instanceof NestingShape) {
            NestingShape s = (NestingShape) parent;
            index = s.shapeCount();
        }
        return index;
    }

    @Override
    public boolean isLeaf(Object node) {
        if (!(node instanceof NestingShape)) {
            return true;
        }
        return false;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int index = -1;

        if (parent instanceof NestingShape) {
            NestingShape s = (NestingShape) parent;
            index = s.indexOf((Shape) child);
        }
        return index;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        _treeModelListeners.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        _treeModelListeners.remove(l);
    }

}
